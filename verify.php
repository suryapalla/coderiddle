<?php 
/* Verifies registered user pinNumber, the link to this page
   is included in the register.php pinNumber message 
*/
require 'db.php';
session_start();

// Make sure pinNumber and hash variables aren't empty
if(isset($_GET['pinNumber']) && !empty($_GET['pinNumber']) AND isset($_GET['hash']) && !empty($_GET['hash']))
{
    $pinNumber = $con->escape_string($_GET['pinNumber']); 
    $hash = $con->escape_string($_GET['hash']); 
    
    // Select user with matching pinNumber and hash, who hasn't verified their account yet (active = 0)
    $result = $con->query("SELECT * FROM users WHERE pinNumber='$pinNumber' AND hash='$hash' AND active='0'");

    if ( $result->num_rows == 0 )
    { 
        $_SESSION['message'] = "Account has already been activated or the URL is invalid!";

        header("location: error.php");
    }
    else {
        $_SESSION['message'] = "Your account has been activated!";
        
        // Set the user status to active (active = 1)
        $con->query("UPDATE users SET active='1' WHERE pinNumber='$pinNumber'") or die($con->error);
        $_SESSION['active'] = 1;
        
        header("location: success.php");
    }
}
else {
    $_SESSION['message'] = "Invalid parameters provided for account verification!";
    header("location: error.php");
}     
?>