<?php
    /* Displays user information and some useful messages */
    session_start();
    require 'db.php';

    // Check if user is logged in using the session variable
    if ( $_SESSION['logged_in'] != 1 ) {
      $_SESSION['message'] = "You must log in!";
      header("location: error.php");    
    }
    else {
        // Makes it easier to read
        $first_name = $_SESSION['first_name'];
        $last_name = $_SESSION['last_name'];
        $pinNumber = $_SESSION['pinNumber'];
        $active = $_SESSION['active'];
    }
    $_SESSION['level'] = 'easy';
    $query = "SELECT riddleId ,level FROM problemsSolved WHERE pinNumber = '$pinNumber' AND level = ( SELECT currentLevel FROM users WHERE pinNumber = '$pinNumber')";
    $result = $con->query($query);
    if ($result->num_rows>0) {
        $result = $result->fetch_array();
        $id = $result['riddleId'];
    }
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>

    <!-- CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/main.css" />
	<link rel="stylesheet" href="css/themify-icons.css">
    <style type="text/css">
        .completed{
            background-color: #1ab188;
        }
        .default{
            background-color: #13232f73;
        }
        .default:hover{
            background-color: #13232fe6;
            color: #fcfcfc;
        }
        .btn{
            width: 20vw;
            height: 20vh;
            margin: 20px;
            color: #fcfcfc;
            font-size: 30px;
        }
        h1{
            padding-top: 20px;
        }
    </style>
</head>
<body>

    <!-- Navigation -->
    <nav class="main-nav menu-dark menu-transparent js-transparent">
        <div class="container">
            <div class="navbar">

                <div class="brand-logo">
                    <a class="navbar-brand" href="#">
                        <img src="images/logo/logo-white2.png" alt="Exort">
                    </a>
                </div>
                <!-- brand-logo -->
            </div>
            <!-- /navbar -->
        </div>
        <!-- /container -->
    </nav>

    <!--/#Navigation-->
    <section>
    <div id="firstDiv" style="background-color: #13232fe6;position: relative;margin-top: 0px;margin-bottom: 0px;height: 100vh;">
                <div class="hero-caption caption-center caption-height-center container" style="z-index: 100;">
                    <h1 class="hero-heading text-light text-xform-none ls-1 font-weight-bold" style="z-index:100">Code Riddle</h1>
                    <p class="hero-text-alt text-light">Show your best. Be the best.
                        <br class="hide-mobile"> Click hear to start your riddles</p>
                    <a id="go" class="btn btn-style-3 btn-lg mrg-top-30">Let's GO</a>
                </div>
    </div>
    </section>

    <section  style="background-color: #fcfcfc">
        <div id="game" style="height: auto;margin-left: 30px;">
            <?php
                $i=1;
                $alphabet = array('1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
                while($i!=36){
                    $option = $alphabet[$i-1]
            ?>
                <a href="getRiddle.php?selectedOption=<?php echo $option ?>" class="btn <?php if($id==$i){echo 'completed';}else{echo 'default';}?>"><h1><?php echo $option;?></h1></a>
            <?php
                    $i = $i+1;
                }
            ?>
        </div>
    </section>

 <!-- Back to top -->
    <a href="#" id="back-to-top" title="Back to top"><i class="ti-angle-up"></i></a>
    <!-- /Back to top -->

    <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/menu.js"></script>
	

</body>
</html>