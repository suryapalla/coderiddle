<?php
/* Displays user information and some useful messages */
session_start();

// Check if user is logged in using the session variable
if ( $_SESSION['logged_in'] != 1 ) {
  $_SESSION['message'] = "You must log in before viewing your profile page!";
  header("location: error.php");    
}
else {
    // Makes it easier to read
    $first_name = $_SESSION['first_name'];
    $last_name = $_SESSION['last_name'];
    $pinNumber = $_SESSION['pinNumber'];
    $active = $_SESSION['active'];
}
?>
<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Welcome <?= $first_name.' '.$last_name ?></title>
  <?php include 'css/css.html'; ?>
</head>

<body>
    <!-- Navigation -->
    <nav class="main-nav menu-dark menu-sticky js-transparent">
        <div class="container">
            <div class="navbar">

                <div class="brand-logo">
                    <a class="navbar-brand" href="#">
                        <img src="images/logo/logo-white2.png" alt="Exort">
                    </a>
                </div>
                <!-- brand-logo -->
            </div>
            <!-- /navbar -->
        </div>
        <!-- /container -->
    </nav>

    <!--/#Navigation-->
  <div class="form">

          <h1>Welcome</h1>
          
          <p>
          <?php 
     
          // Display message about account verification link only once
          if ( isset($_SESSION['message']) )
          {
              echo $_SESSION['message'];
              
              // Don't annoy the user with more messages upon page refresh
              unset( $_SESSION['message'] );
          }
          
          ?>
          </p>
          
          <?php
          
          // Keep reminding the user this account is not active, until they activate
          if ( !$active ){
              echo
              '<div class="info">
              Account is unverified, please confirm your pinNumber by admin!
              </div>';
          }
          else{
            header("location: home.php");
          }
          
          ?>
          
          <h2><?php echo $first_name.' '.$last_name; ?></h2>
          <p><?= $pinNumber ?></p>
          
          <a href="logout.php"><button class="button button-block" name="logout"/>Log Out</button></a>

    </div>
    
<script src='js/jquery-2.1.4.min.js'></script>
<script src="js/index.js"></script>

</body>
</html>
