<?php
/* The password reset form, the link to this page is included
   from the forgot.php pinNumber message
*/
require 'db.php';
session_start();

// Make sure pinNumber and hash variables aren't empty
if( isset($_GET['pinNumber']) && !empty($_GET['pinNumber']) AND isset($_GET['hash']) && !empty($_GET['hash']) )
{
    $pinNumber = $con->escape_string($_GET['pinNumber']); 
    $hash = $con->escape_string($_GET['hash']); 

    // Make sure user pinNumber with matching hash exist
    $result = $con->query("SELECT * FROM users WHERE pinNumber='$pinNumber' AND hash='$hash'");

    if ( $result->num_rows == 0 )
    { 
        $_SESSION['message'] = "You have entered invalid URL for password reset!";
        header("location: error.php");
    }
}
else {
    $_SESSION['message'] = "Sorry, verification failed, try again!";
    header("location: error.php");  
}
?>
<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Reset Your Password</title>
  <?php include 'css/css.html'; ?>
</head>

<body>
    <!-- Navigation -->
    <nav class="main-nav menu-dark menu-sticky js-transparent">
        <div class="container">
            <div class="navbar">

                <div class="brand-logo">
                    <a class="navbar-brand" href="#">
                        <img src="images/logo/logo-white2.png" alt="Exort">
                    </a>
                </div>
                <!-- brand-logo -->
            </div>
            <!-- /navbar -->
        </div>
        <!-- /container -->
    </nav>

    <!--/#Navigation-->
    <div class="form">

          <h1>Choose Your New Password</h1>
          
          <form action="reset_password.php" method="post">
              
          <div class="field-wrap">
            <label>
              New Password<span class="req">*</span>
            </label>
            <input type="password"required name="newpassword" autocomplete="off"/>
          </div>
              
          <div class="field-wrap">
            <label>
              Confirm New Password<span class="req">*</span>
            </label>
            <input type="password"required name="confirmpassword" autocomplete="off"/>
          </div>
          
          <!-- This input field is needed, to get the pinNumber of the user -->
          <input type="hidden" name="pinNumber" value="<?= $pinNumber ?>">    
          <input type="hidden" name="hash" value="<?= $hash ?>">    
              
          <button class="button button-block"/>Apply</button>
          
          </form>

    </div>
<script src='js/jquery-2.1.4.min.js'></script>
<script src="js/index.js"></script>

</body>
</html>
