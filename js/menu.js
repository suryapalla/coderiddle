
    $(window).on('scroll', function() {
        if ($(window).scrollTop() > 1) {
            $('.main-nav').addClass('menu-sticky');
            $('.main-nav.js-transparent').removeClass('menu-transparent');
            $('.main-nav.has-top-nav').removeClass('mrg-top-40');
            $('.main-nav.js-transparent.has-top-logo').removeClass('mrg-top-90');
            $('#back-to-top').addClass('show');
            $('.main-nav.float-menu').removeClass('float-menu-style');
            $('.top-nav,.top-logo').addClass('push-top');
            $('.screen-frame .bar-top').addClass('after-scroll');

        } else {
            $('.main-nav').removeClass('menu-sticky');
            $('.main-nav.js-transparent').addClass('menu-transparent');
            $('.main-nav.has-top-nav').addClass('mrg-top-40');
            $('.main-nav.js-transparent.has-top-logo').addClass('mrg-top-90');
            $('#back-to-top').removeClass('show');
            $('.main-nav.float-menu').addClass('float-menu-style');
            $('.top-nav,.top-logo').removeClass('push-top');
            $('.screen-frame .bar-top').removeClass('after-scroll');
        }
    });

    $('#back-to-top').on('click', function() {
        $('html, body').animate({
            scrollTop: $("#firstDiv").offset().top - 5
        }, 1000);
        return false;
    });

    $("#go").click(function() {
        $('html, body').animate({
            scrollTop: $("#game").offset().top -80
        },2000)
    });




