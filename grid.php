<?php
/* Displays user information and some useful messages */
session_start();

// Check if user is logged in using the session variable
if ( $_SESSION['logged_in'] != 1 ) {
  $_SESSION['message'] = "You must log in";
  header("location: error.php");    
}
else {
    // Makes it easier to read
    $first_name = $_SESSION['first_name'];
    $last_name = $_SESSION['last_name'];
    $pinNumber = $_SESSION['pinNumber'];
    $active = $_SESSION['active'];
}
?>
<?php
  require 'db.php';
  $id = $_SESSION['data']['optionId'];
  $level = $_SESSION['lev'];
  $query = "SELECT R.iconPath , R.id, R.iconName FROM riddles R WHERE R.optionId = '$id' AND level = '$level'";
  $result = $con->query($query);
?>
<!DOCTYPE html>
<html>
<head>
  <title></title>
    <!-- CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/themify-icons.css">
    <link rel="stylesheet" type="text/css" href="css/grid.css">
    
    <style type="text/css">
      body {
        background-color: #2f3238;
        color: #f5f5f5;
      }
      #warning{
        color: red;
        font-size: 17px;
        font-weight: lighter;
        text-align: center;
      }
    </style>
</head>
<body>
    <!-- Navigation -->
    <nav class="main-nav menu-dark menu-sticky js-transparent">
        <div class="container">
            <div class="navbar">

                <div class="brand-logo">
                    <a class="navbar-brand" href="#">
                        <img src="images/logo/logo-white2.png" alt="Exort">
                    </a>
                </div>
                <!-- brand-logo -->
            </div>
            <!-- /navbar -->
        </div>
        <!-- /container -->
    </nav>

    <!--/#Navigation-->
  <div class='container2' style="margin-top: 100px;">
    <main>
<?php
  if ($result->num_rows>0) {
    $i=0;
    while($r = mysqli_fetch_array($result)){
      if($i%3==0){
?>
      <div class='items'>
      <?php } ?>
        <div data-id='<?php echo $r["id"]; ?>' class='item'>
          <svg preserveAspectRatio='xMidYMid slice' viewBox='0 0 300 200'>
            <defs>
              <clipPath id='clip-<?php echo $i; ?>'>
                <circle cx='0' cy='0' fill='#000' r='150px'></circle>
              </clipPath>
            </defs>
            <text class='svg-masked-text' dy='.3em' x='50%' y='50%'>
              <?php echo $r["iconName"]; ?>
            </text>
            <g clip-path='url(#clip-<?php echo $i; ?>)'>
              <image height='100%' preserveAspectRatio='xMinYMin slice' width='100%' xlink:href='<?php echo 'admin/'.$r['iconPath']; ?>'></image>
              <text class='svg-text' dy='.3em' x='50%' y='50%'>
                <?php echo $r["iconName"]; ?>
              </text>
            </g>
          </svg>
          <?php
            if($i%3==2){
              echo "</div>";
            }
          ?>
        </div>
<?php
  $i++;
    }
  }
  else{
        echo "<p id='warning'>Sorry no questions is available for this option</p>";
    }
?>
      </div>
    </main>
  </div>


<script type="text/javascript" src="js/grid.js"></script>
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<script type="text/javascript">
  $('.item').click(function() {
    var data = $(this).attr('data-id');
    alert(data);
    var location = "riddle.php?id="+data;
    window.location.href = location;
  });
</script>
</body>
</html>