
    	$(document).ready(function() {
    		$('.button-holder').click(function() {
    			$(".file2").trigger("click");
    		});
    		$('.file2').on("click",function(e) {
    			e.stopPropagation();
    		});
    		$('.file2').change(function(e) {
    			var fileName = e.target.files[0].name;
    			$('input:disabled').val(fileName); 
    		});
    		$('#editor').click(function() {
    			$('.container1').addClass("closeForm");
    			$('.container1').removeClass("openForm");
    			$('.container2').removeClass("closeWrapper");
    			$('.container2').addClass("openWrapper");
    			$('#overLay').css("display","block");
    		});
    		$('#overLay').click(function() {
    			$('.container1').removeClass("closeForm");
    			$('.container1').addClass("openForm");
    			$('.container2').addClass("closeWrapper");
    			$('.container2').removeClass("openWrapper");
    			$('#overLay').css("display","none");
    		})
    	});