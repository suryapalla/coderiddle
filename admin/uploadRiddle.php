<?php
	$selectedOption = $_POST['alphabet'];
	require '../db.php';
	$query = "SELECT * FROM availableOptions WHERE optionId = '$selectedOption'";
	$result = $con->query($query);
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		#textarea{
			height: 100px;
			width: 500px;
			margin-top: 10px;
		}
		
		select{
			margin-top: 10px;
		}

		.form-control{
			position: relative;
			display: block;
			margin-left: 30%;
		}

		input{
			display: block;
			margin-top: 10px;
		}
	</style>
</head>
<body>
		<?php
			if ($result->num_rows>0) {
				$data = mysqli_fetch_array($result);
		?>
			<h4>There is <?php echo $data[2]; ?> options available for <?php echo $selectedOption; ?></h4>
		<?php
			}
		?>
		<form class="form-control" method="POST" action="insert.php" enctype="multipart/form-data">
			<input type="file" name="image">
			<textarea id="textarea" name="riddle" placeholder="Enter the Riddle" ></textarea>
			<input style="display: none;" type="text" name="option" value="<?php echo $selectedOption ?>" >
			<input style="display: none;" type="text" name="count" value="<?php echo $data[2]; ?>" >
			<input type="text" name="iconName" placeholder="Enter Icon Name">
			<select name="level">
				<option value="Easy">Easy</option>
				<option value="Moderate">Moderate</option>
				<option value="Difficult">Difficult</option>
			</select>
			<input type="submit" value="Submit">
		</form>
</body>
</html>
