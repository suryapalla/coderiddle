<?php 
/* Reset your password form, sends reset.php password link */
require 'db.php';
session_start();

// Check if form submitted with method="post"
if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) 
{   
    $pinNumber = $con->escape_string($_POST['pinNumber']);
    $result = $con->query("SELECT * FROM users WHERE pinNumber='$pinNumber'");

    if ( $result->num_rows == 0 ) // User doesn't exist
    { 
        $_SESSION['message'] = "User with that pinNumber doesn't exist!";
        header("location: error.php");
    }
    else { // User exists (num_rows != 0)

        $user = $result->fetch_assoc(); // $user becomes array with user data
        
        $pinNumber = $user['pinNumber'];
        $hash = $user['hash'];
        $first_name = $user['first_name'];

        // Session message to display on success.php
        $_SESSION['message'] = "<p>Please check your pinNumber <span>$pinNumber</span>"
        . " for a confirmation link to complete your password reset!</p>";

        // Send registration confirmation link (reset.php)
        $to      = $pinNumber;
        $subject = 'Password Reset Link ( clevertechie.com )';
        $message_body = '
        Hello '.$first_name.',

        You have requested password reset!

        Please click this link to reset your password:

        http://localhost/login-system/reset.php?pinNumber='.$pinNumber.'&hash='.$hash;  

        mail($to, $subject, $message_body);

        header("location: success.php");
  }
}
?>
<!DOCTYPE html>
<html>
<head>
  <title>Reset Your Password</title>
  <?php include 'css/css.html'; ?>
</head>

<body>
    <!-- Navigation -->
    <nav class="main-nav menu-dark menu-sticky js-transparent">
        <div class="container">
            <div class="navbar">

                <div class="brand-logo">
                    <a class="navbar-brand" href="#">
                        <img src="images/logo/logo-white2.png" alt="Exort">
                    </a>
                </div>
                <!-- brand-logo -->
            </div>
            <!-- /navbar -->
        </div>
        <!-- /container -->
    </nav>

    <!--/#Navigation-->   
  <div class="form">

    <h1>Reset Your Password</h1>

    <form action="forgot.php" method="post">
     <div class="field-wrap">
      <label>
        pinNumber Address<span class="req">*</span>
      </label>
      <input type="pinNumber"required autocomplete="off" name="pinNumber"/>
    </div>
    <button class="button button-block"/>Reset</button>
    </form>
  </div>
          
<script src='js/jquery-2.1.4.min.js'></script>
<script src="js/index.js"></script>
</body>

</html>
