<?php
	session_start();
	require 'db.php';
	$id = $_SESSION['data']['optionId'];
    $level = $_SESSION['level'];
	$query = "SELECT R.iconPath , R.id FROM riddles R WHERE R.optionId = '$id' AND level = '$level'";
	$result = $con->query($query);
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<!-- CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/main.css" />
	<link rel="stylesheet" href="css/themify-icons.css">
</head>
<body>
<nav class="main-nav menu-dark menu-transparent js-transparent">
        <div class="container">
            <div class="navbar">

                <div class="brand-logo">
                    <a class="navbar-brand" href="#">
                        <img src="images/logo/logo-white2.png" alt="CodeRiddle">
                    </a>
                </div>
                <!-- brand-logo -->
            </div>
            <!-- /navbar -->
        </div>
        <!-- /container -->
    </nav>
<?php
	if ($result->num_rows>0) {
		while($r = mysqli_fetch_array($result)){

?>
	<a href="riddle.php?id=<?php echo $r['id']; ?>"><img src="coderiddles.dx.am/<?php echo 'admin/'.$r['iconPath']; ?>"></a>
<?php
		}
	}
    else{
        echo "<p>Sorry no questions is available for this option</p>";
    }
?>



 <!-- Back to top -->
    <a href="#" id="back-to-top" title="Back to top"><i class="ti-angle-up"></i></a>
    <!-- /Back to top -->

    <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/menu.js"></script>
</body>
</html>